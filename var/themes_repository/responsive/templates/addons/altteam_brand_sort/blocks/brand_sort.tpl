<div id="block_brand_sort">
    <br>
    <h2 class="ty-mainbox-simple-title">{__("brand_sort")}</h2>
    <div class="ty-sc-customer-brand-sort">
        {assign var="curl" value=$config.current_url|fn_query_remove:"brand_sort"}
        {assign var="pagination_id" value=$id|default:"pagination_contents,block_brand_sort"}
        {assign var="ajax_class" value="cm-ajax cm-ajax-full-render"}
        {assign var="brand_search" value=$search}
        {$obj_prefix = "`$block.block_id`"}

        <br>
        {if !empty($sl_search)}
            {assign var="brand_search" value=$sl_search_params}
        {/if}
        {if $block.properties.outside_navigation == "Y"}
            <div class="owl-theme ty-owl-controls">
                <div class="owl-controls clickable owl-controls-outside" id="owl_outside_nav_{$block.block_id}">
                    <div class="owl-buttons">
                        <div id="owl_prev_{$obj_prefix}" class="owl-prev"><i class="ty-icon-left-open-thin"></i></div>
                        <div id="owl_next_{$obj_prefix}" class="owl-next"><i class="ty-icon-right-open-thin"></i></div>
                    </div>
                </div>
            </div>
        {/if}
        
        <div id="scroll_list_{$block.block_id}" class="owl-carousel">
            {foreach fn_get_all_brands() as $brand}
                {include file="common/image.tpl" assign="object_img" class="{if !$brand.variant_id|in_array:$brand_search.brand_sort}ty-grayscale{/if}" image_width=75 image_height=75 images=$brand.image_pair no_ids=true lazy_load=true obj_id="scr_`$block.block_id`000`$brand.variant_id`"}
                <div class="ty-center {if $brand.variant_id|in_array:$brand_search.brand_sort}grey_back{/if}">
                    <a class="brandSortData {$ajax_class} {if $brand.variant_id|in_array:$brand_search.brand_sort}checked_brand{/if}" {if $brand.variant_id|in_array:$brand_search.brand_sort}onclick="Tygh.$(this).removeClass('checked_brand');"{else}onclick="Tygh.$(this).addClass('checked_brand');"{/if}  data-ca-target-id="{$pagination_id}"> <input class="hidden" type="text" value="{$brand.variant_id}" />{$object_img nofilter}</a>
                </div>
            {/foreach}
        </div>
        
        <br>

        <div class="clear_filter_brand_sort">
             <a class="{$ajax_class}"  href="{"`$curl`&brand_sort=C"|fn_url}" data-ca-target-id="{$pagination_id}">{__("clear_filter")}</a>
        </div>

        <a class="{$ajax_class} hidden" id="target_link_{$block.block_id}" href="{"`$curl`"|fn_url}" data-ca-target-id="{$pagination_id}">Link</a>
        {if !empty($sl_search)}
            {include file="addons/altteam_brand_sort/common/scroller_and_brand_sort.tpl" items=$brands prev_selector="#owl_prev_`$obj_prefix`" next_selector="#owl_next_`$obj_prefix`" sl_search="Y"}
            {else}
            {include file="addons/altteam_brand_sort/common/scroller_and_brand_sort.tpl" items=$brands prev_selector="#owl_prev_`$obj_prefix`" next_selector="#owl_next_`$obj_prefix`" sl_search="N"}
        {/if}
    </div>
    <script>
      {* (function(_, $) {
          function fn_check()
        }(Tygh, Tygh.$)); *}
    </script>
<!--block_brand_sort--></div>