<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
 ****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

function fn_altteam_brand_sort_get_companies(&$params, &$fields, &$sortings, &$condition, $join, $auth, $lang_code, $group)
{
    if (!empty($params['brand_sort'])) {
        if ($params['brand_sort'] == 'C') {
            $sortings['brand'] = '?:companies.company';
        }

        $brand_companies = fn_get_brands_company_ids($params['brand_sort'], $lang_code);
        $condition .= db_quote(' AND ?:companies.company_id IN(?a)', $brand_companies);

        $params['brand_sort'] = explode(',',$params['brand_sort']);
    }
}

function fn_altteam_brand_sort_get_store_locations_before_select(&$params, $fields, $joins, &$conditions, &$sortings, $items_per_page, $lang_code)
{
    if (!empty($params['brand_sort'])) {
        if ($params['brand_sort'] == 'C') {
            $sortings['brand'] =  '?:store_locations.position asc, ?:store_location_descriptions.name';
        }

        $brand_companies = fn_get_brands_company_ids($params['brand_sort'], $lang_code);
        $conditions['brand_sort'] = db_quote('company_id IN (?a)', $brand_companies);

        $params['brand_sort'] = explode(',',$params['brand_sort']);
    }
}

function fn_get_brands_company_ids($brand_ids, $lang_code)
{
    static $init_cache = false;

    $brand_ids_data = explode(',', $brand_ids);
    $cache_condition = ['products'];
    $cache_name = 'brand_sort';

    $company_ids = [];
    $company_data_ids = [];
    
    if (!$init_cache && empty(Registry::get($cache_name))) {
        Registry::registerCache($cache_name, $cache_condition, Registry::cacheLevel('static'), true);
        $init_cache = true;
    }

    foreach ($brand_ids_data  as $brand_id) {
        if (!empty(Registry::get($cache_name . '.' . $brand_id))) {
            $company_data_ids[$brand_id] = Registry::get($cache_name . '.' . $brand_id);
        } else {
            $product_ids = db_get_fields("SELECT DISTINCT product_id FROM ?:product_features_values WHERE variant_id = ?i AND lang_code = ?s", $brand_id, $lang_code);
            if (!empty($product_ids)) {
                $company_data_ids[$brand_id] = db_get_fields("SELECT company_id FROM ?:products WHERE product_id IN (?a)", $product_ids);
                Registry::set($cache_name . '.' . $brand_id, $company_data_ids[$brand_id]);
            }
        }
    }

    array_walk_recursive($company_data_ids, function ($company_id) use (&$company_ids) {
        $company_ids[] = $company_id;    
    });

    return  $company_ids;
}
