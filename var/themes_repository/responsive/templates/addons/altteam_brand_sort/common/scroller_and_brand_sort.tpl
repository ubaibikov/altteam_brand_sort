{script src="js/lib/owlcarousel/owl.carousel.min.js"}
<script type="text/javascript">
(function(_, $) {
    //[Alt-team]:brand_sort
    if("{$sl_search}"){
         $("#store_locator_search_form").append('<input type="text" name="sl_search[brand_sort]" class="hidden brandSortAppendData"/><input type="hidden" name="result_ids" value="store_locator_search_controls,store_locator_location,store_locator_search_block_*,block_brand_sort" />'); 
    }
    // [/Alt-team]
    $.ceEvent('on', 'ce.commoninit', function(context) {
        var elm = context.find('#scroll_list_{$block.block_id}');

        $('.ty-float-left:contains(.ty-scroller-list),.ty-float-right:contains(.ty-scroller-list)').css('width', '100%');
        
        //[Alt-team]:brand_sort
        var targetLink = context.find("#target_link_{$block.block_id}");
        var brandSortData = elm.find(".brandSortData");

        $(brandSortData).click(function(){
            var brandSortData = [];

            $('.checked_brand').each(function(i, el){
                brandSortData[i] = $(el).children(":first").val();
            });
            
            if("{$sl_search}" == 'Y'){
                $('.brandSortAppendData').val(brandSortData.join());
                fn_past_form_data($("#store_locator_search_form"));
            }else{
                current_url = $(targetLink).attr('href') + "&brand_sort=" + brandSortData.join();
                $(targetLink).attr('href', current_url).trigger('click');
            }
        });
        
        function fn_past_form_data($form){
            form_data = $form.serializeObject();
            $.ceAjax('request', $form.attr('action'), {
                caching: false,
                data: form_data,
            });            
        }
        // [/Alt-team]

        var item = {$block.properties.item_quantity|default:5},
            // default setting of carousel
            itemsDesktop = 4,
            itemsDesktopSmall = 3;
            itemsTablet = 2;

        if (item > 3) {
            itemsDesktop = item;
            itemsDesktopSmall = item - 1;
            itemsTablet = item - 2;
        } else if (item == 1) {
            itemsDesktop = itemsDesktopSmall = itemsTablet = 1;
        } else {
            itemsDesktop = item;
            itemsDesktopSmall = itemsTablet = item - 1;
        }

        var desktop = [1199, itemsDesktop],
            desktopSmall = [979, itemsDesktopSmall],
            tablet = [768, itemsTablet],
            mobile = [479, 1];

        {if $block.properties.outside_navigation == "Y"}
        function outsideNav () {
            if(this.options.items >= this.itemsAmount){
                $("#owl_outside_nav_{$block.block_id}").hide();
            } else {
                $("#owl_outside_nav_{$block.block_id}").show();
            }
        }
        {/if}

        if (elm.length) {
            elm.owlCarousel({
                direction: '{$language_direction}',
                items: item,
                itemsDesktop: desktop,
                itemsDesktopSmall: desktopSmall,
                itemsTablet: tablet,
                itemsMobile: mobile,
                {if $block.properties.scroll_per_page == "Y"}
                scrollPerPage: true,
                {/if}
                {if $block.properties.not_scroll_automatically == "Y"}
                autoPlay: false,
                {else}
                autoPlay: '{$block.properties.pause_delay * 1000|default:0}',
                {/if}
                lazyLoad: true,
                slideSpeed: {$block.properties.speed|default:400},
                stopOnHover: true,
                {if $block.properties.outside_navigation == "N"}
                navigation: true,
                navigationText: ['{__("prev_page")}', '{__("next")}'],
                {/if}
                pagination: false,
            {if $block.properties.outside_navigation == "Y"}
                afterInit: outsideNav,
                afterUpdate : outsideNav
            });

              $('{$prev_selector}').click(function(){
                elm.trigger('owl.prev');
              });
              $('{$next_selector}').click(function(){
                elm.trigger('owl.next');
              });

            {else}
            });
            {/if}

        }
    });
}(Tygh, Tygh.$));
</script>
