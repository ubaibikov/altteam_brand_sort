<?php
/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
 ****************************************************************************/

$schema['brand_sort'] = array(
    'templates' => 'addons/altteam_brand_sort/blocks/brand_sort.tpl',
    'wrappers' => 'blocks/wrappers',
    'is_managed_by' => ['ROOT'],
    'cache' => false,
    'settings' =>  [
        'not_scroll_automatically' => [
            'type' => 'checkbox',
            'default_value' => 'Y'
        ],
        'speed' => [
            'type' => 'input',
            'default_value' => 400
        ],
        'scroll_per_page' => [
            'type' => 'checkbox',
            'default_value' => 'Y'
        ],
        'pause_delay' => [
            'type' => 'input',
            'default_value' => 3
        ],
        'item_quantity' => [
            'type' => 'input',
            'default_value' => 7
        ],
        'thumbnail_width' => [
            'type' => 'input',
            'default_value' => 75
        ],
        'outside_navigation' => [
            'type' => 'checkbox',
            'default_value' => 'Y'
        ],
        'total_items' => [
            'type' => 'input',
            'default_value' => 0
        ]
    ]

);
return $schema;