<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
 ****************************************************************************/


use Tygh\Tygh;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'search') {
    $sl_search =  Tygh::$app['view']->getTemplateVars('sl_search');

    list($store_locations, $search) = fn_get_store_locations($sl_search);
    
    Tygh::$app['view']->assign([
        'sl_search_params' => $search,
    ]);
}